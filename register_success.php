<?php
include_once 'register.inc.php';
include_once 'logfunctions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="../js/sha512.js"></script> 
        <script type="text/JavaScript" src="../js/forms.js"></script>
		<link href="../css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
		<div class="container">
			<?php include('nav.php');?>
			<div class="dataArea">
				<!-- Registration form to be output if the POST variables are not
				set or if the registration script caused an error. -->
				<div class='sidebar'>
					<div class="menu2" id="floatdiv">
						<em>
								Usernames may contain only digits, upper and lower case letters and underscores<br/>
								Emails must have a valid email format<br/>
								Passwords must be at least 6 characters long<br/>
									Passwords must contain
										<ul>
											<li>At least one upper case letter (A..Z)</li>
											<li>At least one lower case letter (a..z)</li>
											<li>At least one number (0..9)</li>
										</ul>
								Your password and confirmation must match exactly
								</em>
					</div>
				</div>
				<div class="contentarea">
					<h1>Register with us</h1>
						<h2>Thank you for registering.  You may now proceed to login</h2>					
				</div>
				<br style="clear:both;"/>
			</div>
		</div>
		<?php include('../login.php');?>
    </body>
</html>



