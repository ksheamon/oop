<header>
	<div class="leftHead">
		<a href="http://127.0.0.1/test"><img alt="Blue Fountain Media" src="http://www.bluefountainmedia.com/images/logo-desctop.png" width="360" height="60"></a>
	</div>
	<div class="rightHead">
		<?php if ($logged=="in") {?>
			<a href="http://127.0.0.1/test/includes/logout.php">Logout</a> | <a href="http://127.0.0.1/test/admin">Admin</a>
		<?php } else { ?>
			<a href="#" onclick="showLogin()">Login</a>
		<?php }?>
	</div>
</header>

<div class='menu'>
	<a href="http://127.0.0.1/test/leadership.php">Leadership</a>
	<a href="http://127.0.0.1/test/development.php">Development</a>
	<a href="http://127.0.0.1/test/design.php">Design</a>
	<a href="http://127.0.0.1/test/marketing.php">Marketing</a>
	<a href="http://127.0.0.1/test/human_resources.php">Human Resources</a>
	<a href="http://127.0.0.1/test/finance.php">Finance</a>
	<a href="http://127.0.0.1/test/sales.php">BC/Sales</a>
	<a href="http://127.0.0.1/test/accounts.php">Accounts</a>
	<a href="http://127.0.0.1/test/creative_strategy.php">Creative Strategy</a>
	<a href="http://127.0.0.1/test/information_technology.php">Information Technology</a>
</div>