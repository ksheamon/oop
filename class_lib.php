<?php

class leader{
	var $title, $name, $leaderdesc, $leadresp, $pic;
	
	function __construct($title, $name, $leaderdesc, $leadresp, $pic){
		$this->title = $title;
		$this->name = $name;
		$this->leaderdesc = $leaderdesc;
		$this->leadresp = $leadresp;
		$this->pic = $pic;	
	}
	
	function get_title(){
		return $this->title;
	}
	
	function get_leader_name(){
		return $this->name;
	}
	
	function get_leader_desc(){
		return $this->leaderdesc;
	}
	
	function get_lead_resp(){
		return $this->leadresp;
	}
	
	function get_pic(){
		return $this->pic;
	}
}

class department{
	var $name, $description, $hod, $jobs, $jobTitle, $jobDesc, $jobResps;
	
	function __construct($department_name, $description, $hod_name, $hod_pic, $alljobs){
		$this->name = $department_name;
		$this->description = $description;
		$this->hod = new hod($hod_name, $hod_pic);
		$this->jobs = array();
			
		for($x=0; $x<count($alljobs); $x++){
			$jobTitle = $alljobs[$x][0];
			$jobDesc = $alljobs[$x][1];
			$jobResps = $alljobs[$x][2];
		
			$this->jobs[$x] = new job($jobTitle, $jobDesc, $jobResps);
		}
	}
	
	public function get_name(){
		return $this->name;
	}

	function get_description(){
		return $this->description;
	}

}


class hod extends department{
	var $hod_name;
	var $description;
	var $pic;
	
	function __construct($hod_name, $hod_pic){
		$this->hod_name = $hod_name;
		$this->pic = $hod_pic;
	}
	
	function get_hod_name(){
		return $this->hod_name;
	}
	
	function get_hod_description(){
		return $this-> description;
	}
	
	function get_hod_pic(){
		return $this->pic;
	}
		
}


class job extends department{
	var $name;
	var $description;
	var $responsibilities;
	
	function __construct($job_title, $job_desc, $job_resps){
		$this->name = $job_title;
		$this->description = $job_desc;
		$this->responsibilities = $job_resps;
	}
	
	function get_job_title(){
		return $this->name;
	}
	
	function get_job_desc(){
		return $this->description;
	}
	
	function get_job_resps(){
		return $this->responsibilities;
	}
	
}


?>