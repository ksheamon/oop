<?php
require_once("class_lib.php");
include("functions.php");

$leadercount = count_rows('leadershipdept');
$deptcount = count_rows('departments');

$leaders = array();
$depts = array();

for ($n=1; $n<=$deptcount; $n++){
	$deptname = get_dept_name($n);
	$desc = get_dept_desc($n);
	$hodname = get_hod_name($n);
	$hodpic = get_hod_pic($n);
	$alljobs = get_dept_jobs($deptname);		
	
$depts[$n]=new department($deptname, $desc, $hodname, $hodpic, $alljobs);
	
}

for ($x=1; $x<=$leadercount; $x++){
	$jobtitle = get_leader_info($x, 'Title');
	$name = get_leader_info($x, 'Name');
	$leaderdesc = get_leader_info($x, 'Description');
	$pic = get_leader_info($x, 'Pic');
	
	$leaders[$x]=new leader($jobtitle, $name, $leaderdesc, $leadresp, $pic);
}
	
	
?>