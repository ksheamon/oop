-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2014 at 05:55 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `secure_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(1, '1404316602');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`) VALUES
(1, 'test_user', 'test@example.com', '00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', 'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef'),
(2, 'ksheamon', 'ksheamon@gmail.com', '7a6bc7d6517eb18847929e4a69159b7ed181113ee9f55732386654f88c0bef06fc7c4f4a092143a16a5e9e98ef18d8143b403bfdf6d0f3067a98c4f37a86b942', 'b3804b09e5d20015b279ccae31b52e4ebd8f4eaa54e9d4479750f1a93b6dc65e818be687384041e98bac3cebe704e2fcab1af69eda22fa581c1a3be084d5affe'),
(3, 'user1', 'test@test.com', 'a5c165b683edf086bd0b9f18ac9d675c487b518712c9274439e71c9bae9a62e0221eaaca0a7735f566b848581f60e9fa012215316332ab809a7b73c58004abdd', '527d93372571b9d1e6b15aa42e6a2fb3f94c2bb7cbb9327eae591e8c2f744c9ede6ef612413369de7b2ad13e1436d6ed44b2b7279b4b61d19b33846f1102d485'),
(4, 'user2', 'test2@test.com', 'cd4ea25d3d783331d9f313f4fc0a0c1576fd71c9af8170f7215c2bcf855ec9a0e4a60697c5a102373869c045c288a0f1240607d8c2fceed94269191784ad61b7', '3beb797ec769069b6d536b89764d22ceaaf89062010971e381468aaa424559575e44c17cfc5beb84b9fefff47c6cbfb12ee20fd32847a8b1814b1909c2751441'),
(5, 'test3', 'test3@test.com', 'c66e9561476870db19fd79ce92c8085e67f6cf74d9a98d8a82f912f0135086111b6699cf48c81b3ed16c6159bd80eb9b2751b79f0b6abb0c776a6a0217c29d20', 'e516bb707709820ebc2405298a7c974290cfe490e63730c806f4e602c290cb184d0ee74fc94e840da4ead2926a1a7b8c645823320a30a6584d42422962f290cc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
