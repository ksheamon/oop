<?php include('includes/logphp.php')?>
<!DOCTYPE html>
<html>
<head>
<?php require_once("init.php");?>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div class="container">
<?php include('includes/nav.php');
	$i = 8;
?>


<div class="dataArea">
	<?php include('includes/sidebar.php');?>
	
	<div class="leaders"><ul>
		<?php for($n=1; $n<=count($leaders); $n++ ){ ?>
			<li><div class="admin"><strong><?php echo $leaders[$n]->get_title();?>  : <?php echo $leaders[$n]->get_leader_name();?></strong><br/><br/><img src="images/<?php echo $leaders[$n]->get_pic();?>"/><br/>
			<?php echo $leaders[$n]->get_leader_desc();?><br/><br/>
			</div></li>
		<?php } ?>
	</ul></div>

<br style="clear:both;"/>
</div>
<?php include('includes/footer.php');?>
</body>
</html>