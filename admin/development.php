<?php require_once('includes/adminheader.php');
$page = "Development";?>

<body>

<div class="container">
<?php include('includes/adminnav.php');
	$i = 4;
?>


<div class="dataArea">
	<?php include('includes/sidebar.php');?>
	
	<div class="contentarea">
		<?php include('includes/content.php');?>
	</div>

<br style="clear:both;"/>
<a class="scrollup" href="#">Return To Top</a>
</div>
<?php include('includes/footer.php');?>
</body>
</html>