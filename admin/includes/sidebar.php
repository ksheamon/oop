<div class='sidebar'>
	<h1><?php echo $depts[$i]->get_name();?></h1>
	<a class="live" href="<?php echo $live;?>" target="_blank">View Live Page</a>
	<div class="menu2" id="floatdiv">
		<div class="desc">
			<?php echo $depts[$i]->get_description();?><br/><br/>
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<input type="hidden" name="id" value="<?php echo $i;?>"/>
				<label><strong>Update Department Description:</strong></label><br/>
				<textarea name="desc"></textarea>
				<input type="submit" name="updateDesc" value="Update"/>
			</form>
		</div><br/>
		<strong>Jump To:</strong><br/>
		<?php 
		if ($page == "leadership"){
			for ($n=1; $n<=count($leaders); $n++){
				echo '<a href="#'.$leaders[$n]->get_title().'">'.$leaders[$n]->get_title().'</a><br/>';
				//echo $n;
			};
		} else { 
			for ($n=0; $n<(count($depts[$i]->jobs)); $n++){
				echo '<a href="#'.$depts[$i]->jobs[$n]->get_job_title().'">'.$depts[$i]->jobs[$n]->get_job_title().'</a><br/>';
			};
		}; ?>
		
	</div>
</div>