<header>
	<div class="leftHead">
		<a href="http://127.0.0.1/test"><img alt="Blue Fountain Media" src="http://www.bluefountainmedia.com/images/logo-desctop.png" width="360" height="60"></a>
	</div>
	<div class="rightHead">
		<?php if ($logged=="in") {?>
			<a href="http://127.0.0.1/test/admin/includes/logout.php">Logout</a> | <a href="http://127.0.0.1/test/admin/admin">Admin</a>
		<?php } else { ?>
			<a href="#" onclick="showLogin()">Login</a>
		<?php }?>
	</div>
</header>

<div class='menu'>
	<a href="http://127.0.0.1/test/admin/leadership.php">Leadership</a>
	<a href="http://127.0.0.1/test/admin/development.php">Development</a>
	<a href="http://127.0.0.1/test/admin/design.php">Design</a>
	<a href="http://127.0.0.1/test/admin/marketing.php">Marketing</a>
	<a href="http://127.0.0.1/test/admin/human_resources.php">Human Resources</a>
	<a href="http://127.0.0.1/test/admin/finance.php">Finance</a>
	<a href="http://127.0.0.1/test/admin/sales.php">BC/Sales</a>
	<a href="http://127.0.0.1/test/admin/accounts.php">Accounts</a>
	<a href="http://127.0.0.1/test/admin/creative_strategy.php">Creative Strategy</a>
	<a href="http://127.0.0.1/test/admin/information_technology.php">Information Technology</a>
</div>