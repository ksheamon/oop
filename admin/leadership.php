<?php require_once('includes/leaderHeader.php');
$page = "leadership";?>
<body>

<div class="container">
<?php include('includes/adminnav.php');
	$i = 8;
?>


<div class="dataArea">
	<?php include('includes/sidebar.php');?>
	
	<div class="contentarea">
		<?php include('includes/leaderContent.php');?>
	</div>

<br style="clear:both;"/>
<a class="scrollup" href="#">Return To Top</a>
</div>
<?php include('includes/footer.php');?>
</body>
</html>