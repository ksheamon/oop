<?php include('includes/logphp.php')?>
<!DOCTYPE html>
<html>
<head>
<?php require_once("init.php");?>
<link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div class="container">
<?php include('includes/nav.php');
	$i = 10;
?>


<div class="dataArea">
	<?php include('includes/sidebar.php');?>
	
	<div class="contentarea">
		<?php include('includes/content.php');?>
	</div>

<br style="clear:both;"/>
</div>
<?php include('includes/footer.php');?>
</body>
</html>